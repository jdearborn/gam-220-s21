#include <iostream>
using namespace std;

const char* gSomething = "12345678901234567890";

int main(int argc, char* argv[])
{
	// Pointer arithmetic

	const char* something = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	int* myPtr;  // Can store a memory address

	// Try to print uninitialized (garbage) memory
	//cout << *myPtr << endl;

	myPtr = (int*)30;  // Invalid, but some arbitrary memory address value
	myPtr = nullptr;  // Invalid, consistent value

	int a;  // Stack-allocated variable...  has a memory address

	myPtr = &a;

	// Dereference pointer to access that variable
	*myPtr = 7;


	myPtr = new int;  // Allocate new memory on the heap

	// Try to print uninitialized (garbage) memory
	cout << *myPtr << endl;

	*myPtr = 0;
	cout << *myPtr << endl;


	// const char* myString = "Hello";
	char myString[] = { 'H', 'e', 'l', 'l', 'o', '\0' };

	char* strPtr = myString;

	cout << *strPtr << endl;  // Prints H

	strPtr++;
	cout << *strPtr << endl;  // Prints e


	for (int i = 0; i < 5; ++i)
	{
		cout << myString[i];
	}
	cout << endl;

	for (char* p = myString; *p != '\0'; ++p)
	{
		cout << *p;
	}
	cout << endl;

	strPtr++;
	strPtr -= 2;


	//cin.get();


	// Bad time.

	cout << "Commence bad stuff." << endl;

	cout << strPtr << endl;


	while (true)
	{
		char c = cin.get();

		if(c == 'a')
			strPtr -= 20;
		else if(c == 'd')
			strPtr += 20;

		for (int i = 0; i < 20; ++i)
		{
			cout << strPtr[i];
		}
		cout << endl;
	}




	return 0;
}