#include <iostream>
#include <vector>
using namespace std;

void Fn(int arraySize)
{
	int someNumber = 0;

	// Pointer: Stores a memory address (where data can live)
	int* someOtherNumber = new int;  // `new` requests memory of a certain size (int takes 4 bytes)

	// C# `new` is for creating objects (of a class or struct type)
	//Vector3 position = new Vector3(0f, 0f, 4f);
	//GameObject go = Instantiate(...);

	//int myArray[arraySize];  // Can't do this... :(
	int* myArray = new int[arraySize];
	if (arraySize > 10)
	{
		myArray[0] = 222;
		myArray[1] = 8765;
		myArray[5] = 57;
	}

	delete someOtherNumber;
	delete[] myArray;
}

int main(int argc, char* argv[])
{
	/*list<int> myList;
	myList.push_back(1);
	myList.push_back(2);
	myList.push_back(3);
	myList[2] = 0;  // Can't use a list like an array! (because it isn't one)
	*/

	vector<int> myVector;  // What about C#?  THIS IS C# LIST.
	myVector.push_back(1);
	myVector.push_back(2);
	myVector.push_back(3);
	//myVector.reserve(200); // Make sure there is room for this many (preallocate)
	//myVector.resize(200);  // Add enough elements to make the size this many.
	myVector[2] = 0;  // supports random access!  yay!
	cout << myVector.size() << endl;

	string names[100];

	// Prompt user for number of names...
	/*if(numNames > 100)
	{
		cout << "That's too many names! << endl;
		return 1;
	}*/

	Fn(100);

	return 0;
}