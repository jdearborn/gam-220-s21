#include <iostream>
#include <string>
using namespace std;

// Global variables have global access!  Any code can mess with them out from under you.
// Global variables look like any other variable.  They may make code harder to understand.
// Global variables aren't guaranteed to be initialized in any particular order.


void Clear()
{
	for (int i = 0; i < 50; ++i)
		cout << endl;
}

// C++ always defaults to pass-by-value
/*char ToUpper(char c)
{
	if (c >= 'a' && c <= 'z')
		c -= 32;
	return c;
}*/

// Pass-by-reference using... references!
void ToUpper(char& c)
{
	if (c >= 'a' && c <= 'z')
		c -= 32;
}

// Could use pointers for pass-by-reference
/*void ToUpper(char* c)
{
	if (*c >= 'a' && *c <= 'z')
		*c -= 32;
}*/


int main(int argc, char* argv[])
{
	/*string myString;

	// Setting a string
	myString = "Stuff I want here";

	getline(cin, myString);

	// C# chars are 16-bit (2-byte)
	// C++ chars are 8-bit (1-byte)
	// Follows ASCII conventions
	cout << sizeof(char) << endl;

	//char c = 33;
	char c = '!';

	cout << "A character: " << c << endl;

	myString[0] = 'F';

	cout << myString << endl;

	for (size_t i = 0; i < myString.size(); ++i)
	{
		cout << myString[i] << endl;
	}*/



	cout << "The size of one boolean in memory: " << sizeof(bool) << " bytes" << endl;
	cout << "The size of one char in memory: " << sizeof(char) << " bytes" << endl;
	cout << "The size of one int in memory: " << sizeof(int) << " bytes" << endl;

	string myString = "Something";  // Assign using a "string literal"
	// string literals are character arrays
	// A string in memory: 'S', 'o', 'm', 'e', 't', 'h', 'i', 'n', 'g', '\0'
	// \0 (ASCII 0) is the null terminator, denotes the end of a string

	const char* somethingString = "Something";

	char other[] = {'B', 'a', 'd'};
	/*for (int i = 0; i < 10; ++i)
	{
		other[i] = somethingString[i];
	}*/

	//other[9] = '!';

	cout << "That string: " << other << endl;


	cin.get();



	string currentString = "Jon";

	bool quit = false;
	while (!quit)
	{
		
		cout << "Please choose an option:" << endl;
		cout << "a) Display String" << endl
			<< "b) Set String" << endl
			<< "j) Quit" << endl;

		cout << ">";

		string input;
		getline(cin, input);
		if (input.size() > 0)
		{
			// Switch is like if, else-if
			switch (input[0])
			{
			case 'A':
			case 'a':  // 97
				Clear();
				cout << "Current string is: " << currentString << endl;
				cin.get();
				break;
			case 'b':
				Clear();
				cout << "Please enter a new string." << endl;
				cout << ">";
				getline(cin, currentString);
				cout << "Set string to: " << currentString << endl;
				cin.get();
				break;
			case 'c':
				break;
			case 'd':
				break;
			case 'e':  // 101
				break;
			case 'g':
				// Loop through each character...
				// If the character is a space, then capitalize the next letter

				for (int i = 0; i < currentString.size(); ++i)
				{
					if (i == 0)
					{
						//currentString[i] = ToUpper(currentString[i]);
						ToUpper(currentString[i]);
					}
					if (currentString[i] == ' ')
					{
						if (i + 1 < currentString.size())
						{
							if (currentString[i + 1] >= 'a' && currentString[i + 1] <= 'z')
								currentString[i + 1] -= 32;
						}
					}
				}
				break;
			case 'h':
				// Gimme an index

				{
					getline(cin, input);
					//int index = (int)"Hello";  // Scary!
					int index = stoi(input);
					if (islower(currentString[index]))
						currentString[index] = toupper(currentString[index]);
					else if(isupper(currentString[index]))
						currentString[index] = tolower(currentString[index]);
					
				}
				break;
			case 'j':  // 106
				quit = true;
				break;
			}
		}
	}


	// So dangerous!  Don't use GOTO
/*	int i = 0;
loop_34:
	++i;
	cout << i << endl;
	if(i < 10)
		goto loop_34;*/


	return 0;
}