#include <iostream>
#include <random>
#include "Random.h"
using namespace std;

//typedef int MyNumberThing;





int main(int argc, char* argv[])
{
	// Mersenne Twister
	std::mt19937 generator;

	generator.seed(1000);

	std::uniform_int_distribution<int> distribution(3, 17);  // (inc. low, exc. high)
	int someNum = distribution(generator);


	std::uniform_real_distribution<float> floatDistribution(0.0f, 100.0f);
	float someDecimal = floatDistribution(generator);


	cout << "i: " << someNum << ", f: " << someDecimal << endl;

	// C# Unity: Random.Range(0.0f, 100.0f)...  That's so nice!



	Random random;

	cout << random.Integer(5, 23) << endl;





	cin.get();

	return 0;
}