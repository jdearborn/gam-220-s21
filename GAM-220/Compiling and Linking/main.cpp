#include <iostream>
#include <string>
//#include "myheader.h"
using namespace std;

// x86 (32-bit processor, 32-bit (4-byte) memory addresses)
// x64 (64-bit processor, 64-bit (8-byte) memory addresses)

// The amount of addressable memory space is different between 32 and 64 bit architectures.
// 2 ^ 32 = 4,294,967,296 -> ~4,000,000,000 bytes? -> 4GB
// 2 ^ 64 = 18,446,744,073,709,551,616 -> 18 exabytes?!

// How big is a 4k texture?  4,096 x 2,304 = 9437184 pixels
// 8-bit per channel (32-bit) RGBA -> 4 bytes per pixel -> 37,748,736 bytes (37 MB) for ONE texture

// The amount of data processed in one instruction is different.


int main(int argc, char* argv[])
{
    cout << "Please type your name:" << endl;
    cout << ">";

    string name;
    cin >> name;

    cout << "Your name is " << name << endl;

    cin.get();
    cin.get();

    return 0;
}