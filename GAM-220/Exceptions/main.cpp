#include <iostream>
#include <stdexcept>
#include <string>
using namespace std;


int GetValue(int* array, int size, int index)
{
	if (index < 0 || index > size-1)
	{
		// Bad!
		throw std::out_of_range("GetValue() was given an index that was out of bounds: " + to_string(index) + ", array size: " + to_string(size));
	}
	return array[index];
}

int main(int argc, char* argv[])
{
	int myArray[] = {0, 405, 404, 300};
	int size = 4;

	try
	{
		cout << GetValue(myArray, size, -4) << endl;
		cout << GetValue(myArray, size, 4000) << endl;
	}
	catch (const exception& e)
	{
		cout << "Exception! " << e.what() << endl;
	}


	int* a = nullptr;

	if (5 == 7)
	{
		a = new int();
		*a = 5;
	}

	try
	{
		if (a == nullptr)
			throw std::logic_error("a was null!!!");
		else
			throw runtime_error("The connection to the server was lost!");

		if(true)
			throw out_of_range("Tried to access enemy array out of bounds.");

		cout << *a << endl;
	}
	catch (logic_error e)
	{
		cout << "An exception occurred!" << endl;
		cout << e.what() << endl;
		/*if (e == 1)
		{
			// Restart the game
		}*/
	}
	catch (runtime_error e)
	{
		cout << "This was a runtime error! " << e.what() << endl;
	}
	catch (const exception& e)
	{
		cout << "Something went wrong. " << e.what() << endl;
	}
	catch (...)
	{

	}

	cin.get();

	return 0;
}