#include <iostream>
#include <string>
#include <chrono>
using namespace std;
using namespace chrono;

void DoTiming1()
{
	high_resolution_clock::time_point startTime = high_resolution_clock::now(); // "9:42:56:237am 1/20/2021"

	cout << "Hello World!" << endl;

	high_resolution_clock::time_point stopTime = high_resolution_clock::now();


	nanoseconds totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	float numSeconds = totalNS.count() / 1e9f;

	cout << "Time taken: " << numSeconds << endl;
}

void DoTiming2()
{
	high_resolution_clock::time_point startTime = high_resolution_clock::now(); // "9:42:56:237am 1/20/2021"

	cout << "Hello World!" << endl;

	high_resolution_clock::time_point stopTime = high_resolution_clock::now();


	nanoseconds totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	float numSeconds = totalNS.count() / 1e9f;

	cout << "Time taken: " << numSeconds << endl;
}

int main(int argc, char* argv[])
{
	int x = 0;
	// Do stuff with x
	x = 5;
	// Do more stuff with x


	high_resolution_clock::time_point startTime = high_resolution_clock::now(); // "9:42:56:237am 1/20/2021"

	cout << "Hello World!" << endl;

	high_resolution_clock::time_point stopTime = high_resolution_clock::now();


	nanoseconds totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	float numSeconds = totalNS.count() / 1e9f;

	cout << "Time taken: " << numSeconds << endl;




	startTime = high_resolution_clock::now(); // "9:42:56:237am 1/20/2021"

	// Put something to measure here
	/*for (int i = 0; i < 100000; ++i)
	{

	}*/

	string response;
	//cin >> response;
	std::getline(cin, response);

	stopTime = high_resolution_clock::now();


	totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	numSeconds = totalNS.count() / 1e9f;

	cout << "THAT TOOK YOU " << numSeconds << " to type " << response << "!!!  I CAN'T BELIEVE IT." << endl;



	DoTiming1();
	DoTiming2();

	return 0;
}