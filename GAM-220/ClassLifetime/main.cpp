#include <iostream>
using namespace std;

class Currency
{
public:
	Currency()
	{
		cout << "Default Constructor" << endl;
		value = 0;
	}

	Currency(int newValue)
	{
		cout << "Single parameter constructor" << endl;
		value = newValue;
	}

	// Copy constructor
	Currency(const Currency& other)
	{
		cout << "Copy constructor" << endl;
		value = other.value;
	}

	~Currency()
	{
		cout << "Destructor" << endl;
	}

	int& Get()
	{
		cout << "Getter" << endl;
		return value;
	}

	int Get() const
	{
		cout << "Getter" << endl;
		return value;
	}

	void Set(int newValue)
	{
		cout << "Setter" << endl;
		value = newValue;
	}

	// TODO: Chaining
	void operator=(int newValue)
	{
		cout << "operator=" << endl;
		value = newValue;
	}

	void operator=(const Currency& other)
	{
		cout << "Copy assignment operator" << endl;
		value = other.value;
	}

	void operator+=(int amount)
	{
		cout << "operator+=" << endl;
		value += amount;
	}

	Currency operator+(int rhs)
	{
		cout << "operator+" << endl;
		return Currency(value + rhs);
	}

	void operator!()
	{
		cout << "operator!" << endl;
	}

private:
	int value;
};



int main(int argc, char* argv[])
{
	Currency myMoney;

	myMoney.Set(5);

	myMoney.Set(myMoney.Get() + 2);


	myMoney = 5;
	myMoney += 2;
	myMoney = myMoney + 2;
	!myMoney;  // Technically legal now, but I don't know what it should do...

	cout << endl;
	cout << "Part 2" << endl;

	// Copy constructor
	//Currency yourMoney(myMoney);
	Currency yourMoney = myMoney;

	/*Currency yourMoney;
	yourMoney = myMoney;  // operator= with a Currency on the left and a Currency on the right
	*/

	cout << yourMoney.Get() << endl;


	cout << "End of main()" << endl;

	return 0;
}