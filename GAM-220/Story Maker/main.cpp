#include <iostream>
#include <string>
using namespace std;

// static "free" functions are restricted to use in this cpp file/compilation unit
static string Prompt(string message)
{
    //static int i = 5;  // static variables stay alive!  Weird!
    cout << message << endl;
    cout << ">";

    string response;
    getline(cin, response);
    return response;
}

int main(int argc, char* argv[])
{
    // static methods (class member functions)
    /*MyClass myObj;

    myObj.DoThing();*/  // non-static (need an instance object)
    //MyClass::DoThing();  // static (no instance needed)




    cout << "Hey there, welcome to Story Maker!" << endl << endl;

    string name, weapon, monster, prize, vacation;
    name = Prompt("Please type your name:");
    weapon = Prompt("Name the weapon you will use:");
    monster = Prompt("What enemy will you fight?");
    prize = Prompt("What will you win from the fight?");
    vacation = Prompt("Where will you go to rest?");

    cout << endl;

    cout << "Once upon a time, there was a mighty fighter named " << name << "." << endl;
    cout << "The world was impressed with this fighter's power and skills." << endl;
    cout << "One day, those skills were put to the test when a " << monster << " crashed through " << name << "'s house." << endl; ;
    cout << name << " jumped into action, grabbing the " << weapon << " from the bedside." << endl; ;
    cout << "With expert precision, " << name << " slew the " << monster << " with a single strike." << endl;
    cout << "From the body of the " << monster << ", " << name << " found a " << prize << "." << endl; ;
    cout << "With the enemy slain, the fighter knew that some rest was in order." << endl; ;
    cout << name << " immediately headed off towards " << vacation << " and lived happily ever after." << endl;

    cin.get();

    return 0;
}