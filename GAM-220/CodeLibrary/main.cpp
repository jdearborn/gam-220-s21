#include "CodeLibrary.h"
#include <string>
#include <fstream>
using namespace std;


void WriteToFile(string filename, string content)
{
	ofstream fout;
	fout.open(filename);

	fout << content;
	fout.close();
}