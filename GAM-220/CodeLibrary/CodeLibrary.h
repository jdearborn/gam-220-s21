#pragma once
#include <string>

// __declspec(dllexport) makes sure the function is exported (saved in) to the DLL file
// __declspec(dllimport) is for when we're using the library

#ifdef BUILD_LIBRARY
	#define DLL_EXPORT __declspec(dllexport)
#else
	#define DLL_EXPORT __declspec(dllimport)
#endif


void DLL_EXPORT WriteToFile(std::string filename, std::string content);
