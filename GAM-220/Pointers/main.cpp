#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	//char myArray[4000000];  // allocate array on the stack
	// char == 1 byte  10101000 (max 255)
	// int == 4 bytes  10101000 10101010 11111111 01110101 (max 4,294,967,296)
	//int* myArray = new int[1000000];  // allocate array on the heap

	// On 64-bit architectures, a memory address is 64 bits (8 bytes)
	// 10101000 10101010 11111111 01110101 10101000 10101010 11111111 01110101 (max 1.8446744e+19)
	// 1K kilo
	// 1M mega
	// 1B giga
	// 1T terabyte
	// 1q petabyte
	// 1Q exabyte

	// Let's push some limits!
	/*for (int i = 0; i < 1000; ++i)
	{
		int* myArray = new int[1000000];  // allocate array on the heap
	}*/

	int a[10];

	int* b = new int[20];

	// Arrays and pointers can be almost perfectly interchangeable (syntax-wise)
	a[0] = 5;
	b[0] = 5;
	int* c = a;

	c[1] = 27;

	cout << a[1] << endl;


	string myString = "Hello!";

	// Get memory address (address-of &)
	cout << "The string \"" << myString << "\" is stored at " << &myString << endl;
	// 006FF7A4, represented in hexadecimal
	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,  A,  B,  C,  D,  E,  F, 10, 11, 12, 13, 14
	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
	string* strPtr = &myString;

	// Dereference to get the stuff at the memory address
	cout << strPtr << *strPtr << endl;

	cout << "Size:\n\t\\ " << myString.size() << " or " << (*strPtr).size() << endl;
	cout << "Size: " << strPtr->size() << endl;
	
	//myObj.member.counter++;  // C#
	//myObj->member->counter++;  // C++


	return 0;
}