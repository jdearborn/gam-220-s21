#include <iostream>
#include "MagicPtr.h"
#include <string>
#include <memory>
using namespace std;

template<typename T>
void MyFn(MagicPtr<T> p)
{
	cout << "Your magic contains: " << *p << endl;
}

int main(int argc, char* argv[])
{
	{
		// Can I apply RAII to raw pointers?!
		int* myPtr = new int;

		delete myPtr;
	}

	int b, c, d, e, f;

	b = 5;

	c = d = 7;

	int* ptr;
	int *otherPtr;

	int a;

	int& myRef = a;
	//int &myRef2 = a;




	// RAII applied to pointers: Smart pointers
	{
		MagicPtr<int> myPtr(new int);

		// deletes automatically!!!
	}


	MagicPtr<int> mp(new int);
	*mp = 4;

	cout << *mp << endl;


	MyFn(mp);
	MyFn(mp);
	MyFn(mp);
	MyFn(mp);
	MyFn(mp);



	{
		MagicPtr<string> myString(new string);

		*myString = "Hello";

		MyFn(myString);
	}

	// Shallow copy: Copying just the values of member variables (e.g. pointer addresses)

	// Deep copy: Making a copy of the values pointed at, not just their memory addresses

	int* array1 = new int[5]{0, 1, 2, 3, 4};

	int* array2 = array1;  // Shallow

	// Deep copy
	int* array3 = new int[5];
	for (int i = 0; i < 5; ++i)
	{
		array3[i] = array1[i];
	}


	unique_ptr<int> myUniquePtr(new int(6));

	shared_ptr<int> mySharedPtr = make_shared<int>(6);

	shared_ptr<int> sharedPointer2 = mySharedPtr;

	cin.get();

	return 0;
}