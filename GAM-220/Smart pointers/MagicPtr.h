#pragma once

template<typename T>
class MagicPtr
{
public:
	MagicPtr(T* newPtr)
	{
		ptr = newPtr;
	}

	MagicPtr(const MagicPtr& other)
	{
		// "Unique" pointer
		/*ptr = other.ptr;
		other.ptr = nullptr;*/

		// A deep copy
		ptr = new T;
		*ptr = *other.ptr;

		// Can we share???  Reference counting?
	}

	// Rule of Three: If you use one of these, you need all three: Destructor, Copy Constructor, Copy Assignment Operator
	~MagicPtr()
	{
		delete ptr;
	}


	void operator=(const MagicPtr& other)
	{
		delete ptr;

		// A deep copy
		ptr = new T;
		*ptr = *other.ptr;
	}
	
	/*MagicPtr& operator=(const MagicPtr& other)
	{
		...
		return *this;
	}*/


	T* operator->()
	{
		return ptr;
	}

	// Memory address?  Is it useful?
	// One very useful thing: IDENTITY
	// TODO: Equality comparison between MagicPtrs?

	// Dereference: Unary operator: *A
	T& operator*()
	{
		return *ptr;
	}

	/*
	//  Multiplication: Binary operator: A * B
	int operator*(int a)
	{
		return *ptr * a;
	}*/

private:
	T* ptr;
};