#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// "Modern" C++
// RAII - Resource Acquisition Is Initialization
void MyFn()
{
	ofstream fout("myData.csv");

	fout << "Hello, there!" << endl;
	fout << "This is a file, bro." << endl;
}

void PrintFileToConsole(string filename)
{
	ifstream fin(filename);
	cout << "Printing " << filename << "..." << endl;
	while (fin.good())
	{
		string line;
		getline(fin, line);

		cout << line << endl;
	}
}

void MakeData(int maxLines)
{
	ofstream fout("data.txt");

	for (int t = 0; t < maxLines; ++t)
	{
		fout << t << '\t' << (2 * t) << '\t' << t * t << endl;
	}
}


int main(int argc, char* argv[])
{
	MyFn();

	PrintFileToConsole("myData.csv");
	//PrintFileToConsole("Turtle2.bmp");
	//PrintFileToConsole("discord-icon.png");

	MakeData(100);

	system("gnuplot sample_plot.txt");

	// Old way / C# way
	/*ofstream fout;
	fout.open("myData.csv");

	fout << "Hello, there!" << endl;
	fout << "This is a file, bro." << endl;

	fout.close();*/

	return 0;
}