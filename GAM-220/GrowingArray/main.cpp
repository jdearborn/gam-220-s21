#include <iostream>
#include <string>
using namespace std;

int GetValue(int* array, int size, int index)
{
	if (index < 0 || index > size - 1)
	{
		// Bad!
		throw std::out_of_range("GetValue() was given an index that was out of bounds: " + to_string(index) + ", array size: " + to_string(size));
	}
	return array[index];
}

void DemoCapacity()
{
	// Size and Capacity are different!
	// Size: How many elements are currently stored/being used
	// Capacity: How big the array actually is


	int capacity = 20;
	int size = 0;

	int* array = new int[capacity];


	// Add an element, the value 5
	if (capacity >= size + 1)
	{
		array[size] = 5;
		++size;
	}
	else
	{
		// Not enough capacity to store one more element
		// TODO: Resize/grow, then add that element
	}





	delete[] array;
}

int main(int argc, char* argv[])
{
	//int myArray[50];
	//int myArray[] = {3, 5, 1, 3, 6};

	// Declaration
	int* myArray;
	int size = 10;

	// Allocation of memory
	myArray = new int[size];

	// Initialization of values
	for (int i = 0; i < size; ++i)
	{
		myArray[i] = 69*i;
	}

	// Try "growing" this array to 15 elements
	// Create a new array of the desired size
	int otherSize = 15;
	int* otherArray = new int[otherSize];

	// Copy the old values to the new array
	/*for (int i = 0; i < size; ++i)
	{
		otherArray[i] = myArray[i];
	}*/
	memcpy(otherArray, myArray, size * sizeof(int));

	// Replace old array by deleting it (the values are safe!) and reassigning the pointer
	delete[] myArray;  // Deallocates the memory for the first array

	myArray = otherArray;
	size = otherSize;  // Update the size


	// Do things...


	// All done...
	delete[] myArray;


	DemoCapacity();


	return 0;
}