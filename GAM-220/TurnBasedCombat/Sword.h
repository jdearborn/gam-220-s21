#pragma once

#include "Weapon.h"
#include <iostream>
#include "Random.h"


class Scimitar : public Weapon
{
public:
	Scimitar(int newPower, int newSpeed)
		: Weapon(newPower, newSpeed, WeaponType::Slashing)
	{}

	virtual std::string GetName() const override
	{
		return "scimitar";
	}

	virtual void DoAttack(Hero& attacker, Hero& defender, Random& rnd) override
	{
		int randomNumber = rnd.Integer(0, 100);
		switch (defender.GetWeapon()->GetType())
		{
			case WeaponType::Slashing:
			case WeaponType::Bludgeoning:
				if (randomNumber < 20)
				{
					std::cout << attacker.GetName() << " swings the " << GetName() << ", but it is blocked!" << std::endl;
					return;
				}
				break;
			case WeaponType::Piercing:
				if (randomNumber < 10)
				{
					std::cout << attacker.GetName() << " swings the " << GetName() << ", but it is blocked!" << std::endl;
					return;
				}
				break;
		}




		//int damage = attacker.GetWeapon()->GetPower();
		Weapon* w = attacker.GetWeapon();
		int damage = w->GetPower();

		// Announce the attack
		std::cout << attacker.GetName() << " swings the " << GetName() << " at ";
		std::cout << defender.GetName() << ", striking for " << damage << " damage!  ";

		// Make it hurt
		defender.Hurt(damage);

		// Say what the effect was
		std::cout << defender.GetName() << " has " << defender.GetHitpoints() << " health left." << std::endl;
	}
};