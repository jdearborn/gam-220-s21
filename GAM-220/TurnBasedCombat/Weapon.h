#pragma once
#include <string>
#include "Hero.h"
#include "Random.h"

enum class WeaponType
{
	Slashing,
	Piercing,
	Bludgeoning
};


// Abstract class that stands as the base class for more specialized weapons
class Weapon
{
public:
	Weapon(int newPower, int newSpeed, WeaponType newType)
	{
		power = newPower;
		speed = newSpeed;
		type = newType;
	}

	int GetPower() const
	{
		return power;
	}
	int GetSpeed() const
	{
		return speed;
	}
	WeaponType GetType() const
	{
		return type;
	}

	// 'virtual' so child classes can override this function
	// '= 0' is to make it "pure virtual" or "abstract"
	virtual std::string GetName() const = 0;

	virtual void DoAttack(Hero& attacker, Hero& defender, Random& rnd) = 0;

protected:
	int power;
	int speed;
	WeaponType type;
};
