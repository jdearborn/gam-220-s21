#include <iostream>
#include "Hero.h"
#include "Sword.h"
using namespace std;



int main(int argc, char* argv[])
{
	Hero hero1("Jon", 125, new Scimitar(35, 10));
	Hero hero2("Quinn", 90, new Scimitar(30, 20));
	Random rnd;

	// TODO: Make a loop where they fight.
	hero1.GetWeapon()->DoAttack(hero1, hero2, rnd);
	hero2.GetWeapon()->DoAttack(hero2, hero1, rnd);

	cin.get();

	return 0;
}