#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char* argv[])
{
	const int numBuckets = 10;
	int bins[numBuckets];
	/*for (int i = 0; i < numBuckets; ++i)
	{
		bins[i] = 0;
	}*/
	memset(bins, 0, numBuckets * sizeof(int));

	int bucketSize = RAND_MAX / numBuckets;

	for (int i = 0; i < 10000; ++i)
	{
		int bucketIndex = (rand() % 20000) / (bucketSize+1);
		bins[bucketIndex]++;
	}

	ofstream fout("data.txt");
	for (int i = 0; i < numBuckets; ++i)
	{
		fout << i << '\t' << bins[i] << endl;
	}

	system("gnuplot sample_plot.txt");

	return 0;
}