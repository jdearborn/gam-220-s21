#include <iostream>
#include <string>
using namespace std;

class A
{
	virtual void fn()
	{}
};

class B : public A
{};


int main(int argc, char* argv[])
{
	// Typecasting: Type conversions

	// Implicit type conversions
	// smaller type to a larger one
	int myInt = 5;
	long long myLong = 5000000000;

	myInt = myLong;


	cout << "Value: " << myInt << endl;

	// Derived pointer to a base pointer (polymorphism)
	B* b = new B();
	A* a = b;



	// Explicit type conversions
	int someInt = stoi("543");
	string myString = to_string(879);

	// Typecasts (explicit)
	char c = 'H';
	cout << "Char: " << c << ", " << (int)c << endl;  // C-style cast

	cout << "Char2: " << 'G' << ", " << int('G') << endl;  // C++-style (function-style) cast

	//(int)(c + 1);
	//int(c + 1);

	int myValue = 56789;
	//cout << "Char3" << 'J' << ", " << (string)'J' << endl;
	int* myPtr = (int*)myValue;
	//cout << *myPtr << endl;

	// static_cast, dynamic_cast, const_cast, reinterpret_cast
	myInt = static_cast<int>(myLong);


	B* player = new B();
	A* someCharacter = player;

	// You must be CERTAIN that someCharacter points to a B
	B* actuallyPlayer = static_cast<B*>(someCharacter);

	// dynamic_cast will check and fail if the cast is unsuccessful.
	// failure means the pointer becomes nullptr
	A* notThePlayer = new A();
	actuallyPlayer = dynamic_cast<B*>(someCharacter);
	actuallyPlayer = dynamic_cast<B*>(notThePlayer);
	if (actuallyPlayer == nullptr)
		cout << "That's not a player!" << endl;

	// const_cast
	// Breaking the const system


	// reinterpret_cast
	// Reinterpret as something else OR shoot yourself in the foot
	int* somePtr = new int();
	void* genericPointer = somePtr;
	somePtr = reinterpret_cast<int*>(genericPointer);


	return 0;
}