#include <iostream>
#include <set>
#include <map>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	set<int> mySet;

	mySet.insert(5);
	mySet.insert(3);
	mySet.insert(3);
	mySet.insert(7);
	mySet.insert(7);
	mySet.insert(1);
	mySet.insert(8);
	mySet.insert(8);

	for (auto e = mySet.begin(); e != mySet.end(); ++e)
	{
		cout << "Set value: " << *e << endl;
	}


	cout << endl << endl;


	map<string, int> myIntegers;

	myIntegers.insert(make_pair("Quinn", 7));

	pair<string, int> zachsPair("Zach", 20);
	myIntegers.insert(zachsPair);

	cout << myIntegers["Quinn"] << endl;

	for (map<string, int>::iterator e = myIntegers.begin(); e != myIntegers.end(); ++e)
	{
		cout << "Map value: " << e->first << " : " << e->second << endl;
	}


	cin.get();

	return 0;
}