#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	// C#
	// Console.WriteLine("Hello World!");
	// Console.Write("Hello World!\n");

	// C++
	cout << "Hello " << "World!" << endl;

	// Without using namespace std
	std::cout << "Hello " << "World!" << std::endl;

	// C
	//printf("Hello %s\n", "World!");

	return 0;
}