#include <iostream>
#include <chrono>
#include <string>
using namespace std;
using namespace chrono;

// Use std::chrono and functions to make a math quiz game



void DoTiming1()
{
	high_resolution_clock::time_point startTime = high_resolution_clock::now(); // "9:42:56:237am 1/20/2021"

	cout << "Hello World!" << endl;

	high_resolution_clock::time_point stopTime = high_resolution_clock::now();


	nanoseconds totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	float numSeconds = totalNS.count() / 1e9f;

	cout << "Time taken: " << numSeconds << endl;
}


void PlaySpeedMath()
{
	cout << "Welcome to Speed Math!" << endl;
	cout << "Ready?  Press Enter to begin..." << endl;
	cin.get();

	// Seed the random number generation, tell it where to start
	srand(time(NULL));

	int correctCount = 0;
	int totalCount = 10;

	// Ask the user 10 RANDOM arithmetic questions and time how long it takes
	high_resolution_clock::time_point startTime = high_resolution_clock::now(); // "9:42:56:237am 1/20/2021"
	
	for (int i = 0; i < totalCount; ++i)
	{
		// Ask one question

		// Randomize these...
		int a = rand() % 11;
		int b = rand() % 11;

		int operNum = rand() % 4;
		char operSym = '+';
		int answer = 0;

		switch (operNum)
		{
		case 0:
			operSym = '+';
			answer = a + b;
			break;
		case 1:
			operSym = '-';
			answer = a - b;
			break;
		case 2:
			operSym = 'x';
			answer = a * b;
			break;
		case 3:
			operSym = '/';
			answer = a / b;
			break;
		}
		cout << a << " " << operSym << " " << b << " = ";
		string input;
		getline(cin, input);

		// Other operations
		int responseAnswer = stoi(input);
		if (responseAnswer == answer)
		{
			cout << "Correct!" << endl;
			correctCount++;
		}
		else
		{
			cout << "Wrong... :(" << endl;
		}
	}
	
	
	high_resolution_clock::time_point stopTime = high_resolution_clock::now();


	nanoseconds totalNS = duration_cast<nanoseconds>(stopTime - startTime);
	float numSeconds = totalNS.count() / 1e9f;

	cout << "You got " << correctCount << " correct out of " << totalCount << endl;
	float correctRatio = correctCount / (float)totalCount;
	if (correctRatio < 0.25f)
	{
		cout << "Oh, Math Goodness.  You stink." << endl;
	}
	else if (correctRatio < 0.5f)
	{
		cout << "You...  need some practice.  Try again, I think." << endl;
	}
	else if (correctRatio > 0.9f)
	{
		cout << "Nice job!" << endl;
	}
	else
	{
		cout << "Good, I guess???" << endl;
	}

	cout << "Time taken: " << numSeconds << endl;


	cout << "Thanks for playing!" << endl;
	cin.get();
}