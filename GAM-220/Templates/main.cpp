#include <iostream>
#include <string>
using namespace std;

/*void Print(string s)
{
	cout << s << endl;
}*/
/*void Print(string& s)
{
	cout << "Ref: " << s << endl;
}*/
/*
void Print(const string& s)
{
	cout << s << endl;
}*/

// This is like a blueprint for creating functions that look like this, but have different types for T.
template<typename T>
void Print(const T& value)
{
	cout << value << endl;
}

template<typename T>
T Divide(const T& a, const T& b)
{
	return a / b;
}





template<typename T>
class Array
{
public:
	// Constructor
	Array() = delete;  // Why?
	// This constructor would make useless objects
	/*Array()
	{
		size = 0;
		data = nullptr;
	}*/

	Array(int arraySize)
	{
		// Maybe throw an exception here?
		size = arraySize;
		data = new T[size];
	}

	// Copy constructor (do a deep copy)
	Array(const Array<T>& other)
	{
		size = other.size;
		//data = other.data; // shallow!

		data = new T[size];
		for (int i = 0; i < size; ++i)
		{
			data[i] = other.data[i];
		}
	}

	// Destructor
	~Array()
	{
		delete[] data;
	}

	void operator=(const Array<T>& other)
	{
		delete[] data;  // Already exists because at this point, some constructor has already run.

		size = other.size;

		data = new T[size];
		for (int i = 0; i < size; ++i)
		{
			data[i] = other.data[i];
		}
	}


	// Getter for size (size, count, length)
	int GetSize() const
	{
		return size;
	}

	// Getter for an element (random access) (Get, At, GetElement)
	T& Get(int index)
	{
		if (index < 0 || index >= size)
			throw std::out_of_range("Array::Get() index out of bounds!");
		return data[index];
	}
	const T& Get(int index) const
	{
		if (index < 0 || index >= size)
			throw std::out_of_range("Array::Get() index out of bounds!");
		return data[index];
	}

	// operator[] (same as above), operator overloading
	T& operator[](int index)
	{
		return Get(index);
	}
	const T& operator[](int index) const
	{
		return Get(index);
	}

private:
	
	// Private constructors can exist
	/*Array()
	{
		size = 0;
		data = nullptr;
	}*/

	// Let other classes access our internals
	//friend class MyOtherClass;

	T* data;
	int size;
};



void DoAThing(Array<int> myArray)
{
	cout << "I like this array, it has a size of " << myArray.GetSize() << endl;
}


void PrintArray(const Array<int>& myArray)
{
	for (int i = 0; i < myArray.GetSize(); ++i)
	{
		cout << myArray[i] << " ";
	}
	cout << endl;
}


int main(int argc, char* argv[])
{
	Print("Hello!");
	//Print<string>("Hello!");

	//string s = "You are cool!";
	string s("You are cool!");

	Print(s);

	Print(5);

	cout <<	Divide(7, 3) << endl;
	Print(Divide<float>(7, 3));
	//Print(Divide((float)6, 3.0f));

	Array<int> myArray(10);

	cout << myArray.GetSize() << endl;
	myArray[0] = 27;  // expression must be a modifiable lvalue
	// "lvalue" means something that can go on the LEFT side of an assignment operator

	int b;
	5;
	b = 7;
	// "rvalue" means something that can go on the RIGHT side of an assignment operator
	b = b + 1;
	b = b;
	b = 1;


	Array<int>* arr2 = new Array<int>(20);
	arr2->Get(0) = 3;
	delete arr2;  // Destructor gets called, then memory is deallocated

	// delete makes pointers invalid
	// To be safe, it is common to null an invalid pointer
	arr2 = nullptr;

	arr2 = new Array<int>(4);
	arr2->Get(0) = 7;
	delete arr2;  // Destructor gets called, then memory is deallocated

	{
		Array<int> arr3(10);

		// Destructor gets called automatically when variable leaves scope
		// Leads us to the concept of RAII, Resource Acquisition Is Initialization
	}
	// Now the array is GONE!




	// Declare
	int fred;
	int* freddina = new int;

	// Initialize
	fred = 0;
	// freddina = new int;
	*freddina = 0;

	// Access (getter): Read fred and use fred
	cout << fred << endl;
	cout << *freddina << endl;

	// Mutate (setter): Write to fred
	fred = 17;
	*freddina = 17;

	// Free / deallocate
	// fred is automatic for "automatic variables"
	delete freddina;



	// Declare / initialize / allocate
	Array<char> bob(20);
	Array<char>* bobbina = new Array<char>(20);
	
	// Access
	cout << bob[0] << endl;
	cout << bobbina->Get(0) << endl;

	// Mutate
	bob[0] = 5;
	bobbina->Get(0) = 5;

	// Free / deallocate
	// Bob is automatic, thanks to our destructor
	delete bobbina;





	Array<int> array1(10);
	for (int i = 0; i < array1.GetSize(); ++i)
	{
		array1[i] = i;
	}

	Array<int> array2(5);
	for (int i = 0; i < array2.GetSize(); ++i)
	{
		array2[i] = 2*i + 1;
	}




	DoAThing(array1);

	DoAThing(array2);

	DoAThing(array1);




	Array<int> array3(array1);  // copy constructor
	Array<int> array4 = array1;  // copy constructor (special case, NOT assignment)

	array2 = array1;  // copy assignment operator
	




	// Problem: Shallow copies!
	cout << "Trying a copy..." << endl;
	int* rawArray1 = new int[5]{4, 2, 6, 3, 6};
	//int* rawArray2 = rawArray1;  // DANGER!  Shallow copy.

	// SAFE!  Do a deep copy instead.
	int* rawArray2 = new int[5];
	for (int i = 0; i < 5; ++i)
	{
		rawArray2[i] = rawArray1[i];
	}

	cout << "delete[] rawArray1" << endl;
	delete[] rawArray1;

	cout << "Setting a value in rawArray2" << endl;
	rawArray2[0] = 1234;
	rawArray2[4] = 1234;
	cout << "Raw array value: " << rawArray2[0] << endl;

	delete[] rawArray2;

	cout << "All done!" << endl;




	// The Rule of Three
	/*
	Whenever you implement a destructor, a copy constructor, or a copy assignment operator...
	You should probably implement ALL THREE.
	*/

	// RAII
	// Resource Acquisition Is Initialization

	// For our Array class, resource acquisition was allocating an array internally (a memory request via `new`).


	//SillyArray<int> mySillyArray;
	//mySillyArray.Init(10);
	//mySillyArray.Free();

	{
		Array<int> array1700(10);  // constructor acquires resources

		// Now as the scope is left, the destructor frees those resources
	}


	{
		// Can I apply RAII to raw pointers?!
		int* myPtr = new int;

		delete myPtr;
	}


	// RAII applied to pointers: Smart pointers
	{
		MagicPtr myPtr(new int);

		// deletes automatically!!!
	}




	cin.get();


	return 0;
}