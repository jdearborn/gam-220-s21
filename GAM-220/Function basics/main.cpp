#include <iostream>
using namespace std;

// FUnctions: Organization, Reuse, Clarity (naming)

// Pass-by-value
void SetTo5AndPrint(int n)
{
	cout << endl << "n before: " << n << endl;
	n = 5;
	cout << "n after: " << n << endl;
}



// Function definitions
// The function with a body / implementation


// Function declaration / function prototype / forward declaration
void SetTo20AndPrint(int& n);


// Function invocation / function calls...  see below


// Instruction pointer / program counter: Keeps track of what the next instruction or line of code will be.



// 1) Using references, write a function called "SwapIntegers" that takes two integer references and 
// exchanges their values.
void SwapIntegers(int& a, int& b)
{
	int x = a;
	a = b;
	b = x;
}


// 2) Write a function called "Fold3" that takes one integer reference and two other integers.  It adds
// all of the integers together and stores the result in the integer reference.  It does not return a value.
void Fold3(int& a, int b, int c)
{
	a = a + b + c;
}



int main(int argc, char* argv[])
{
	int myNum = 0;

	// FUnction call / invocation
	SetTo5AndPrint(myNum);
	cout << "After SetTo5AndPrint(), myNum: " << myNum << endl;

	SetTo5AndPrint(77);



	SetTo20AndPrint(myNum);
	cout << "After SetTo20AndPrint(), myNum: " << myNum << endl;

	//SetTo20AndPrint(8);  // Can't do it!






	return 0;
}





// Pass-by-reference
void SetTo20AndPrint(int& n)
{
	cout << endl << "n before: " << n << endl;
	n = 20;
	cout << "n after: " << n << endl;
}