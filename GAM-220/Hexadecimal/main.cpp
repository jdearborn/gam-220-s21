#include <iostream>
using namespace std;


int main(int argc, char* argv[])
{
	int a;

	int* ptr = &a;
	//int* ptr = (int*)0x56789;

	cout << "0x" << ptr << endl;

	cout << (int)ptr << endl;

	cout << 0x1 << endl;
	cout << 0x2 << endl;
	cout << 0x4 << endl;
	cout << 0x8 << endl;
	cout << 0x10 << endl;
	cout << 0x20 << endl;
	cout << 0x40 << endl;
	cout << 0x80 << endl;
	cout << 0x100 << endl;
	cout << 0x200 << endl;
	cout << 0x400 << endl;
	cout << 0x800 << endl;
	cout << 0x1000 << endl;

	cout << 0b00001000 << endl;

	// Overflow
	cout << "Overflow" << endl;
	unsigned char overflow = 255;
	cout << (int)overflow << endl;
	overflow++;
	cout << (int)overflow << endl;

	// Underflow
	unsigned char underflow = 0;
	underflow--;
	cout << (int)underflow << endl;

	int i = 4000000000;
	long l = 40000000000000;

	float f = 1.0000001f;
	double d = 1.000000000001f;
	//quad q;



	/*Room* roomA = new Room();
	Room* roomB = new Room();
	Room* roomC = new Room();


	roomA->AddNeighbor(roomB);

	roomB->AddNeighbor(roomC);
	roomB->AddNeighbor(roomA);


	Room* currentRoom = startingRoom;

	// Ask player for their choice of room to move to (list neighboring rooms)
	// ...
	currentRoom = currentRoom->GetNeighbor(3);
	*/


	return 0;
}