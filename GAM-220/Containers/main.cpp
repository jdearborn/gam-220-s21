#include <iostream>
#include <vector>
#include <list>
using namespace std;

// Abstract Data Types


int main(int argc, char* argv[])
{
	// What is a vector?
	// 1) Math vector: Direction & magnitude
	// 2) In computing, a "vector" is a resizeable array

	vector<int> myVector;
	myVector.push_back(0);
	myVector.push_back(1);
	myVector.push_back(2);
	myVector.push_back(42);

	// Random access
	// operator[]
	cout << myVector[0] << endl;

	//cout << myVector[678] << endl;

	for (int i = 0; i < myVector.size(); ++i)
	{
		cout << myVector[i] << endl;
	}




	vector<int>* myPtrVector = new vector<int>();
	myPtrVector->push_back(42);

	delete myPtrVector;

	/*
	// C#
	List<int> myList = new List<int>();
	myList.Add(42);
	*/


	// Linked list - NOT AN ARRAY
	// Each element is "linked" (via pointer) to the next element in sequence
	list<int> myList;
	myList.push_back(0);
	myList.push_back(1);
	myList.push_back(2);
	myList.push_back(3);


	// Performance:
	// arrays are contiguous memory and support random access
	// lists... are "scattered" in memory and do not support random access
	//cout << myList[2] << endl;

	// Alright, how do I visit each element in a list?
	// Use an iterator, of course!

	for (list<int>::iterator e = myList.begin(); e != myList.end(); ++e)
	{
		cout << *e << endl;
	}

	// Array/Vector: [4][7][3][7]
	// List: [4]->[7]->[3]->[7]
	// Tree:  [4]
	//        / \
	//      [7] [3]
	//            \
	//            [7]

	for (auto e = myList.begin(); e != myList.end(); ++e)
	{
		cout << *e << endl;
	}

	auto i2 = 4;
	auto f = 6543.0f;

	// range-based "for"
	for (int n : myList)
	{
		cout << n << endl;
	}

	// How do you insert a new value into an existing array?
	int myArray[5] = {0, 1, 2, 3, 4};

	// Replacement?
	//myArray[2] = 7;
	// {0, 1, 7, 3, 4}

	// Want insertion: {0, 1, 7, 2, 3, 4}

	int myOtherArray[6];
	for (int i = 0; i < 6; ++i)
	{
		if (i < 2)
			myOtherArray[i] = myArray[i];
		else if (i > 2)
			myOtherArray[i] = myArray[i - 1];
	}
	myOtherArray[2] = 7;

	for (int i = 0; i < 6; ++i)
	{
		cout << myOtherArray[i] << endl;
	}


	// A vector can do this easily!
	myVector.insert(myVector.begin() + 2, 7);

	// Linked lists are VERY good at insertion and removal.
	myList.insert(myList.begin(), 7);


	return 0;
}