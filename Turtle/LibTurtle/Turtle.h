#pragma once
#include <cstdint>
#include <string>


#ifndef DLL_EXPORT

#ifdef BUILD_LIBRARY
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif

#endif

struct Point
{
	float x = 0.0f;
	float y = 0.0f;

	Point() = default;
	Point(float X, float Y)
		: x(X), y(Y)
	{}
};

struct Color
{
	uint8_t red = 0;
	uint8_t green = 0;
	uint8_t blue = 0;
	uint8_t alpha = 255;

	Color() = default;
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255)
		: red(r), green(g), blue(b), alpha(a)
	{}
};

struct Pen
{
	bool active = false;
	Color color;
	float size = 1.0f;

	Pen() = default;
};

struct TurtleImpl;

class DLL_EXPORT Turtle
{
public:
	Pen pen;

	Turtle();
	~Turtle();

	void SetSimulate(bool enable);

	void Clear();
	void Hide(bool enable);
	void Save(const std::string& filename);

	void Forward(float distance);
	void TurnLeft(float degrees);
	void TurnRight(float degrees);

	void MoveTo(float x, float y);
	void LookAt(float degrees);

	void Sleep(float seconds);

	Point GetPosition() const;
	float GetHeading() const;
	int GetBoundaryWidth() const;
	int GetBoundaryHeight() const;

	bool IsHidden() const;
	bool IsSimulating() const;

	float GetSpeed() const;
	void SetSpeed(float speed = 100.0f);


private:
	TurtleImpl* impl;
};