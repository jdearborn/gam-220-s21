#include "Turtle.h"
#include <iostream>
#include "Random.h"
using namespace std;


int main(int argc, char* argv[])
{
	// C#
	//Console.WriteLine("Hello, {0}!", name);
	//Console.WriteLine($"Hello, {name}!");

	// C++ <string>
	// string myString = to_string(47.349f);
	// C++20 <format>
	// string myString = format("Hello, {}!", name);


	Random random;

	Turtle turtle;

	turtle.MoveTo(turtle.GetBoundaryWidth()/2.0f, turtle.GetBoundaryHeight()/2.0f);

	turtle.pen.active = true;
	turtle.pen.color = Color(255, 0, 0);
	turtle.pen.size = 20;

	for (int i = 100; i > 10; i -= 10)
	{
		for (int j = 0; j < 10; ++j)
		{
			turtle.pen.color = Color(random.Integer(0, 256), random.Integer(0, 256), random.Integer(0, 256));
			turtle.Forward(i * 2.0f / 10.0f);
		}

		turtle.TurnRight(90);
	}

	turtle.Sleep(1.0f);
	turtle.MoveTo(0, 0);

	return 0;
}