#include "Console.h"
#include "Game.h"
using namespace std;


int main(int argc, char* argv[])
{
	cout << "   --Battleship--" << endl;
	cout << "Press Enter to Begin" << endl;
	Console::WaitForEnter();
	Console::Clear();

	Game game;

	game.PlaceShips();

	bool player1Turn = true;
	while(true)
	{
		string name = player1Turn? "Player1" : "Player2";

		Console::Clear();
		cout << name << ", press Enter to continue." << endl;
		Console::WaitForEnter();


		game.PrintBoards(player1Turn);

		int row, column;
		while(!PromptForCoordinates("Choose coordinates to fire (e.g. B7)", row, column))
		{/* empty */}

		FireResult result = game.Fire(player1Turn, row, column);

		game.PrintBoards(player1Turn);

		if (result.hit)
		{
			cout << endl << "HIT!!!" << endl;
			if (result.sank != Ship::None)
				cout << "You sank the " << GetShipString(result.sank) << endl;

			// Check opposite player's board
			if (game.HasNoShips(!player1Turn))
			{
				cout << endl << name + " wins!" << endl;
				break;
			}
		}
		else
			cout << endl << "Miss..." << endl;

		cout << "Press Enter to Continue." << endl;
		Console::WaitForEnter();

		player1Turn = !player1Turn;

	}

	Console::WaitForEnter();

	return 0;
}