#pragma once
#include "Ship.h"

// Used for placing ships
enum class Alignment
{
	Horizontal,
	Vertical
};

// Used to describe the effect a shot had (miss, hit, hit & sank)
struct FireResult
{
	bool hit;
	Ship sank;

	FireResult()
	{
		hit = false;
		sank = Ship::None;
	}

	FireResult(bool Hit, Ship Sank)
	{
		hit = Hit;
		sank = Sank;
	}
};

// Represents the 10x10 grid for each player where ships can be placed and hits/misses can be scored.
class Board
{
public:
	// Initialize data by setting each element of the grid to underscore '_'
	Board();

	// Returns true if (row, column) is legal and the space there is an underscore '_'
	bool IsSpaceOpen(int row, int column) const;

	// Fills the given coordinate with the appropriate character to represent the given ship,
	// then fills adjacent spaces according to the alignment.
	// Returns true if the placement is legal (no obstruction and on the grid), false otherwise.
	bool PlaceShip(Ship ship, int row, int column, Alignment alignment);

	// Marks the given location as either a hit ('X') or a miss ('O').
	// Returns a FireResult saying if it was a hit and if a ship was sunk, which ship it was.
	FireResult FireAt(int row, int column);

	// Returns the character/symbol stored at the given locaiton in the grid.
	char GetSpace(int row, int column) const;

	// Returns true if there are any locations in the grid with the given ship's symbol.
	bool HasShip(Ship ship) const;

	// Returns true if there are no ship symbols remaining in the grid.
	bool HasNoShips() const;

private:
	char grid[100];

	// Changes what is stored in the grid at the given location to the given value.
	void SetSpace(int row, int column, char value);
};
