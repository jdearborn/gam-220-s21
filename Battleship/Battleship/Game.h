#pragma once
#include <string>
#include "Board.h"


bool ParseCoordinates(std::string response, int& row, int& column);
bool PromptForCoordinates(std::string message, int& row, int& column);


class Game
{
public:

	Game();

	bool IsSpaceOpen(bool player1, int row, int column) const;

	bool PlaceShip(bool player1, Ship ship, int row, int column, Alignment alignment);
	void PlaceShips();


	FireResult Fire(bool player1, int row, int column);
	char GetSpace(bool player1, int row, int column) const;

	void PrintBoards(bool player1) const;

	bool HasShip(bool player1, Ship ship) const;
	bool HasNoShips(bool player1) const;

private:
	Board boards[2];

	Board& GetBoard(bool player1);
	const Board& GetBoard(bool player1) const;
};