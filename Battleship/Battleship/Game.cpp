#include "Console.h"
#include "Game.h"
using namespace std;

bool ParseCoordinates(std::string response, int& row, int& column)
{
	// Check that the input is sane

	// Must be 2 or 3 characters (A1, A10)
	if (response.size() < 2 || response.size() > 3)
		return false;

	response[0] = toupper(response[0]);
	// Must start with A-J
	if (response[0] < 'A' || response[0] > 'J')
		return false;
	// Next must be 1-9
	if (response[1] < '1' || response[1] > '9')
		return false;
	// If there are 3 characters, the column must be 10
	if (response.size() == 3 && (response[1] != '1' || response[2] != '0'))
		return false;

	// Convert to 0-9
	row = response[0] - 'A';

	if (response.size() == 3)
		column = 9;
	else
	{
		// Convert to 0-9
		column = response[1] - '1';
	}

	return true;
}

bool PromptForCoordinates(std::string message, int& row, int& column)
{
	bool parseSuccess = false;
	do
	{
		string response = Console::Prompt(message);
		parseSuccess = ParseCoordinates(response, row, column);
	} while (!parseSuccess);
	return true;
}



Game::Game()
{}

bool Game::IsSpaceOpen(bool player1, int row, int column) const
{
	return GetBoard(player1).IsSpaceOpen(row, column);
}

bool Game::PlaceShip(bool player1, Ship ship, int row, int column, Alignment alignment)
{
	return GetBoard(player1).PlaceShip(ship, row, column, alignment);
}

void Game::PlaceShips()
{
	for (int playerNum = 0; playerNum <= 1; ++playerNum)
	{
		Console::Clear();
		bool player1 = (playerNum == 0);
		int shipIndex = 0;
		do
		{
			int row, column;
			bool coordsSuccess = false;
			do
			{
				PrintBoards(player1);
				string name = player1? "Player1" : "Player2";

				coordsSuccess = PromptForCoordinates(name + ", place your " + GetShipString(ships[shipIndex]) + " (e.g. C3)", row, column);
			} while (!coordsSuccess);

			Alignment alignment = Alignment::Horizontal;
			do
			{
				string orientation = Console::Prompt("Place horizontally (H) or vertically (V)?");
				if (orientation.size() != 1)
					continue;
				orientation[0] = toupper(orientation[0]);
				if (orientation[0] == 'H')
				{
					alignment = Alignment::Horizontal;
					break;
				}
				else if (orientation[0] == 'V')
				{
					alignment = Alignment::Vertical;
					break;
				}
			} while (true);

			if (PlaceShip(player1, ships[shipIndex], row, column, alignment))
			{
				shipIndex++;
			}

		} while (shipIndex < 5);
	}
}

FireResult Game::Fire(bool player1, int row, int column)
{
	return GetBoard(!player1).FireAt(row, column);  // Opposite player's board
}

char Game::GetSpace(bool player1, int row, int column) const
{
	return GetBoard(player1).GetSpace(row, column);
}

void Game::PrintBoards(bool player1) const
{
	cout << "   Enemy Side             Your Ships" << endl;

	cout << "   ";
	for (int x = 1; x <= 10; ++x)
	{
		cout << x << ' ';
	}
	cout << "  ";
	for (int x = 1; x <= 10; ++x)
	{
		cout << x << ' ';
	}
	cout << endl;

	char row = 'A';
	for (int y = 0; y < 10; ++y)
	{
		cout << row << " |";
		for (int x = 0; x < 10; ++x)
		{
			char c = GetBoard(!player1).GetSpace(y, x);  // Opposite player's board
			if (c != 'O' && c != 'X')
				cout << '_';
			else
				cout << c;
			cout << " ";
		}

		cout << "| |";

		for (int x = 0; x < 10; ++x)
		{
			cout << GetBoard(player1).GetSpace(y, x);  // Your board
			cout << " ";
		}
		cout << "|" << endl;
		++row;
	}
}

bool Game::HasShip(bool player1, Ship ship) const
{
	return boards[!player1].HasShip(ship);
}

bool Game::HasNoShips(bool player1) const
{
	return boards[!player1].HasNoShips();
}

Board& Game::GetBoard(bool player1)
{
	return boards[!player1];
}

const Board& Game::GetBoard(bool player1) const
{
	return boards[!player1];
}