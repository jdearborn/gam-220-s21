#pragma once

#include <iostream>
#include <string>

namespace Console
{
	static void WaitForEnter()
	{
		while (std::cin.get() != '\n')
		{/* empty */}
	}

	static void Clear()
	{
		for (int i = 0; i < 50; ++i)
			std::cout << std::endl;
	}

	static std::string Prompt(std::string message)
	{
		std::cout << message << std::endl;
		std::cout << ">";

		std::string response;
		std::getline(std::cin, response);
		return response;
	}

}